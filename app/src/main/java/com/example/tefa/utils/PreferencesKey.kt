package com.example.tefa.utils

object PreferencesKey {
    const val IS_FIRST_OPEN = "isFirstOpen"
    const val IS_LOGGED_IN = "isLoggedIn"
    const val USER_TOKEN = "userToken"
}