package com.example.tefa.ui.newestcontent

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.tefa.R

class NewestContentActivity : ComponentActivity() {
    var contents: List<String> = listOf("Irfan Naufal", "Rizky Febrianto", "Test 1", "Test 2")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ContentList(authors = contents)
        }
    }

    @Composable
    @Preview("Sample Recycler")
    fun ContentList(
        modifier: Modifier = Modifier,
        authors: List<String> = List(1000) { "$it" }
    ) {
        LazyColumn(modifier = modifier.padding(vertical = 4.dp)) {
            items(items = authors) { name ->
                ContentBooks(author = name) {
                    Toast.makeText(this@NewestContentActivity, it, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @Composable
    @Preview("Sample Content")
    fun ContentBooks(
        title: String? = "Sample Book 1",
        author: String? = "-",
        selected: ((String) -> Unit)? = null
    ) {
        Card(
            elevation = 8.dp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .clickable { selected?.invoke(author.toString()) },
            shape = RoundedCornerShape(size = 8.dp),
        ) {
            val isFavorite = remember { mutableStateOf(false) }
            val iconFavorite =
                if (isFavorite.value) R.drawable.ic_favorite_filled else R.drawable.ic_favorite

            Column(modifier = Modifier.padding(0.dp, 0.dp, 0.dp, 16.dp)) {
                Image(
                    painter = painterResource(id = R.drawable.banner_asset_2),
                    contentDescription = "content image",
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(135.dp)
                        .clip(RectangleShape),
                    contentScale = ContentScale.Crop
                )
                Row(modifier = Modifier.fillMaxWidth()) {
                    Column(modifier = Modifier.weight(1f)) {
                        Text(
                            "$title",
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(16.dp, 12.dp, 0.dp, 0.dp)
                        )
                        Text(
                            "Author : $author",
                            color = colorResource(id = R.color.grey_text),
                            modifier = Modifier.padding(16.dp, 0.dp)
                        )
                    }
                    Image(
                        painter = painterResource(id = iconFavorite),
                        contentDescription = "favorite content",
                        modifier = Modifier
                            .padding(16.dp, 12.dp, 16.dp, 0.dp)
                            .clickable {
                                isFavorite.value = !isFavorite.value
                            },
                    )
                }
            }
        }
    }
}
