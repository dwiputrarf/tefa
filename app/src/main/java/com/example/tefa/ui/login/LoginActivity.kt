package com.example.tefa.ui.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import androidx.lifecycle.ViewModelProvider
import com.example.tefa.base.BaseActivity
import com.example.tefa.data.remote.response.Login
import com.example.tefa.databinding.ActivityLoginBinding
import com.example.tefa.ui.MainActivity
import com.example.tefa.ui.viewmodel.MainViewModel
import com.kodekita.toaster.ToasterMessage

//  Regita Hutari
class LoginActivity : BaseActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var mainViewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        initObserver()
        initAction()
        setContentView(binding.root)
    }

    private fun initObserver() {
        mainViewModel.isLoading.observe(this) {
            if (it) showLoading() else hideLoading()
        }

        mainViewModel.isLogin.observe(this) {
            if (it.token != null) {
                //Simpan token ke shared preferences
                pref?.isLoggedIn = true
                pref?.userToken = it.token

                // handle success login, pindah ke halaman home
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                // handle failed login, menampilkan pesan error
                ToasterMessage.toaster(this, "Login gagal")
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initAction() {
        with(binding) {
            btnLogin.setOnClickListener {
                val email = binding.etEmail.text.toString()
                val password = binding.etPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()) {
                    ToasterMessage.toaster(
                        this@LoginActivity,
                        "Please input your email and password"
                    )
                } else {
                    val loginRequest = Login.Request(email, password)
                    mainViewModel.login(loginRequest)
                }

            }

            etPassword.setOnTouchListener { v, event ->
                val DRAWABLE_RIGHT = 2
                if (event.action == MotionEvent.ACTION_UP && event.rawX >= (etPassword.right - etPassword.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // toggle password visibility
                    etPassword.transformationMethod =
                        if (etPassword.transformationMethod == PasswordTransformationMethod.getInstance()) HideReturnsTransformationMethod.getInstance() else PasswordTransformationMethod.getInstance()
                    etPassword.text?.let { it1 -> etPassword.setSelection(it1.length) }
                    v.performClick()
                    return@setOnTouchListener true
                }
                false
            }

            ivBack.setOnClickListener {
                onBackPressedDispatcher.onBackPressed()
            }
        }
    }

}