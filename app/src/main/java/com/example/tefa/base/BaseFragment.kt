package com.example.tefa.base

import androidx.fragment.app.Fragment
import com.example.tefa.utils.PreferencesManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class BaseFragment : Fragment() {
    val pref by lazy { PreferencesManager(requireContext()) }
}