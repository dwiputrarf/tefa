package com.example.tefa.base

import androidx.appcompat.app.AppCompatActivity
import com.example.tefa.utils.LoadingUtils
import com.example.tefa.utils.PreferencesManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class BaseActivity : AppCompatActivity() {
    val pref by lazy { applicationContext?.let { PreferencesManager(it) } }

    fun showLoading() = LoadingUtils.showLoading(this)
    fun hideLoading() = LoadingUtils.hideLoading()
    fun isShowLoading() = LoadingUtils.isShowLoading()
}