package com.example.tefa.ui.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.tefa.data.remote.Repository
import com.example.tefa.data.remote.response.Login
import com.example.tefa.utils.CoroutineRule
import com.example.tefa.utils.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = CoroutineRule()

    @Mock
    private lateinit var repository: Repository

    @Mock
    private lateinit var context: Context

    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setUp() {
        mainViewModel = MainViewModel(context, repository)
    }

    @Test
    fun `verify login in ViewModel should worked`() = runTest {
        val dummyData = Login.Request("eve.holt@reqres.in", "cityslicka")
        val dummyResponse = Login.Response("test")

        `when`(repository.login(dummyData)).thenReturn(dummyResponse)
        mainViewModel.login(dummyData)
        val actualData = mainViewModel.isLogin.getOrAwaitValue()

        assertEquals(dummyResponse, actualData)
    }
}